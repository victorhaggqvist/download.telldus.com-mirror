# -*- coding: utf-8 -*-

from socket import *

addr = ("255.255.255.255",3865)
UDPSock = socket(AF_INET,SOCK_DGRAM)
UDPSock.setsockopt(SOL_SOCKET,SO_BROADCAST,1)

#body = string.replace(sys.argv[4],"\\n","\n")

# Send the message
#UDPSock.sendto(msg,addr)


def sendMessage(type, scheme, body):
	bodyString = ''
	for key, value in body.iteritems():
		bodyString = bodyString + "%s=%s\n" % (key, value)
	
	msg = type + "\n{\nhop=1\nsource=telldus-testapp.python\ntarget=*\n}\n" + scheme + "\n{\n" + bodyString + "}\n"
	UDPSock.sendto(msg,addr)

sendMessage('xpl-stat', 'lighting.netinfo', {'device-count': 2, 'status': 'ok', 'name': '@Office'})
sendMessage('xpl-stat', 'lighting.devinfo', {'device': 1, 'channel': 'true,true,0,0', 'status': 'ok', 'name': 'Device with dimmer', 'network': 'default'})
sendMessage('xpl-stat', 'lighting.devinfo', {'device': 2, 'channel': 'true,false,0,0', 'status': 'ok', 'name': 'Non dimmable device', 'network': 'default'})
