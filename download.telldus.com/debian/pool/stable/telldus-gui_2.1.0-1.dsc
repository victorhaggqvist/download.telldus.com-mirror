-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-gui
Binary: libtelldus-gui2, tellduscenter
Architecture: any
Version: 2.1.0-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.com
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7.0.50~), cmake
Checksums-Sha1: 
 642081deed12b96b7308f0afd873ea6c40e615b9 1004408 telldus-gui_2.1.0.orig.tar.gz
 f69902bd947f869080d5c24ff578b9d595620897 9140 telldus-gui_2.1.0-1.debian.tar.gz
Checksums-Sha256: 
 05cd62c4220bd60554a46c5b27b8e7825a63812707ea7bcd18d98d8bb6e38d2c 1004408 telldus-gui_2.1.0.orig.tar.gz
 18bad6539857eaa00ab3448948b8445ac6013b633eeea744b7afc9369f8c3e77 9140 telldus-gui_2.1.0-1.debian.tar.gz
Files: 
 def18d7b02e219080972851f4e2a2b91 1004408 telldus-gui_2.1.0.orig.tar.gz
 495771c600b5f5b328e9f5f9b9c91ebc 9140 telldus-gui_2.1.0-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk57OO8ACgkQWpSRgeUB6naWFgCgxs+s1imNBNGgcoSBsFFtZh5s
Bn8An13aOM9KLr2kmNekKN2vIxmyR+3Y
=K/iq
-----END PGP SIGNATURE-----
