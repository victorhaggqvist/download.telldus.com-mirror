-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-gui
Binary: libtelldus-gui2, tellduscenter
Architecture: any
Version: 2.1.2-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.com
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 7.0.50~), cmake
Package-List: 
 libtelldus-gui2 deb libs extra
 tellduscenter deb utils extra
Checksums-Sha1: 
 0ed37bcc00f3922ead49ef94971467ce87535bfa 1279918 telldus-gui_2.1.2.orig.tar.gz
 ee0861718cc4e153db57a08664c7a1c3e26722ee 2859 telldus-gui_2.1.2-1.debian.tar.gz
Checksums-Sha256: 
 492527365bd397fca608cf34c200424f983915cc1fb3e998fdea71d498aa6702 1279918 telldus-gui_2.1.2.orig.tar.gz
 00f58095b47e184127cf1a6ec95e6b4787fb963d6b6576bbe67ff04ef51abf66 2859 telldus-gui_2.1.2-1.debian.tar.gz
Files: 
 39ac23a7833dd5396e39c0c066c3db53 1279918 telldus-gui_2.1.2.orig.tar.gz
 0b60a6e10b5eabe729114bfa2ebd3bdd 2859 telldus-gui_2.1.2-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iEYEARECAAYFAlM6zKAACgkQWpSRgeUB6nYIfwCfS2NrNrSs+ckhxkOlToejuI4h
pzoAoI52ZlO3PS9zjttmidXCfK2Qz477
=UwZh
-----END PGP SIGNATURE-----
