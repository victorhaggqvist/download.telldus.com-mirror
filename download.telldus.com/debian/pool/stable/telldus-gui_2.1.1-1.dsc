-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-gui
Binary: libtelldus-gui2, tellduscenter
Architecture: any
Version: 2.1.1-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.com
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7.0.50~), cmake
Checksums-Sha1: 
 931794e496be1fb9aac181e9876be1734e851984 1153509 telldus-gui_2.1.1.orig.tar.gz
 f15d6f72e7ac1a3af4d9961a6dd1dd34ae6a4520 3230 telldus-gui_2.1.1-1.debian.tar.gz
Checksums-Sha256: 
 67db46494247456b18b433d79ef2229c8329f37cdf24f2197881e12f433508be 1153509 telldus-gui_2.1.1.orig.tar.gz
 106ca23ed34808451a484cd7fbf32c986f7d4fb3e0288e5de9c8894a3f1222f2 3230 telldus-gui_2.1.1-1.debian.tar.gz
Files: 
 162b3cd37dccc7e797d29a9ca31aad29 1153509 telldus-gui_2.1.1.orig.tar.gz
 f728c453923e5d61e801bf44fb208fbe 3230 telldus-gui_2.1.1-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk8vqP8ACgkQWpSRgeUB6nZ9RACcCk3WP7rhlhUSdCcUGYaIZr++
b0AAoM5yKdAJAqaHgiCMWnRMb1FfRrvI
=M7Ml
-----END PGP SIGNATURE-----
