-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.1.1-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7)
Checksums-Sha1: 
 2cd194caadcc919949987156e7affee8aef54a7b 114498 telldus-core_2.1.1.orig.tar.gz
 4ed2cfe0ea97db81f62c73ccf8082a33f156e6b0 6854 telldus-core_2.1.1-1.debian.tar.gz
Checksums-Sha256: 
 5b470c9337adf954c238160090151c4d2da50fb43a9ab9f3c5ac8e797033c20f 114498 telldus-core_2.1.1.orig.tar.gz
 d195a9fd58d6ac8a638672bf646706d35640241cbda4963401e14ff9c7ed4088 6854 telldus-core_2.1.1-1.debian.tar.gz
Files: 
 5f1dfc03b500671caeb54bfa9f50b9ce 114498 telldus-core_2.1.1.orig.tar.gz
 2b63e78bf1f99ebdb7b74f95b0661d2d 6854 telldus-core_2.1.1-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk8voEwACgkQWpSRgeUB6nbTHACeOnPuVXOslu06JK/Qc8RNOzzj
dxYAmweyfVtz2ejQL49tQinoflvDb4m8
=rFTz
-----END PGP SIGNATURE-----
