-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.1.0-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7)
Checksums-Sha1: 
 a4f59275a4533e1f76d26c89d514ca12251da22e 89679 telldus-core_2.1.0.orig.tar.gz
 be59c7aea05f8bca804f35ab612e9bebda120da0 8237 telldus-core_2.1.0-1.debian.tar.gz
Checksums-Sha256: 
 c8422836063e3aee426007045697a6bc216e6473f933378b40a63ecbec2dbc75 89679 telldus-core_2.1.0.orig.tar.gz
 74db4f4307b65965590d617b88f4d7b86ca25f38e0ba9b53bfa8dd3abe006650 8237 telldus-core_2.1.0-1.debian.tar.gz
Files: 
 c7f6a55281e2237e5b33a0a92eec411e 89679 telldus-core_2.1.0.orig.tar.gz
 91a9767542f28008fe02c891ff639835 8237 telldus-core_2.1.0-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk6mqTsACgkQWpSRgeUB6nbt+gCgi8jHsxXIqr6uEGC+/O+Va8Dg
V5cAoIZa4AFKwTOOwoB5CdC3nzS4R0Yv
=jCwO
-----END PGP SIGNATURE-----
