-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.1.2-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 7)
Package-List: 
 libtelldus-core-dev deb libdevel extra
 libtelldus-core2 deb libs extra
 telldus-core deb libs extra
Checksums-Sha1: 
 c3e54d684508d12718901859774a5462e2dec92f 169850 telldus-core_2.1.2.orig.tar.gz
 5e0be5c31a72c88ac3756bac7b3cb1fd6ba034e9 6751 telldus-core_2.1.2-1.debian.tar.gz
Checksums-Sha256: 
 a20f6c74814afc23312d2c93ebbb37fdea9deaaee05ae7b6a6275e11e4662014 169850 telldus-core_2.1.2.orig.tar.gz
 2b535b1716cf4e9e7cd8642e45aa646ba266e0342d5ac484feca92ad9f96da61 6751 telldus-core_2.1.2-1.debian.tar.gz
Files: 
 9016622b10bc84b275efbffe1874d619 169850 telldus-core_2.1.2.orig.tar.gz
 3304b8260f22706f7decf1a83820008c 6751 telldus-core_2.1.2-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iEYEARECAAYFAlM6xLQACgkQWpSRgeUB6nYU0wCeKWf1wwI2uslLQ0KC9ZWDCUfb
B0QAnjngKQRpjIrb1xr8jIikK2UgJwNm
=j2o3
-----END PGP SIGNATURE-----
