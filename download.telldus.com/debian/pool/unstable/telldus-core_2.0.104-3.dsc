-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.0.104-3
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7)
Checksums-Sha1: 
 c92084c804c8b3db3c5533f903ce778445a97f37 71373 telldus-core_2.0.104.orig.tar.gz
 2b8dc7f7a49e699373a208f64625667b7cedbcbc 8492 telldus-core_2.0.104-3.debian.tar.gz
Checksums-Sha256: 
 959ab56f4038cb3a148d8c364c7ff57fe19c99cb12ba340be9607fde28cd431d 71373 telldus-core_2.0.104.orig.tar.gz
 3d0fe138fc67dfadeaec6eed30c2de7324eb6c37ec200b98da7369ed9d46d210 8492 telldus-core_2.0.104-3.debian.tar.gz
Files: 
 a396de5e8664f54f2a5c718ef6c078f2 71373 telldus-core_2.0.104.orig.tar.gz
 03acd11d727a0319b5c67482512c50b7 8492 telldus-core_2.0.104-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk2QWtsACgkQWpSRgeUB6nZ/swCglakV97NPygDEqEo3QYBoco7o
+sEAnjWFYzY9qYU1BEjzvMBN3zJ58kKb
=rXsT
-----END PGP SIGNATURE-----
