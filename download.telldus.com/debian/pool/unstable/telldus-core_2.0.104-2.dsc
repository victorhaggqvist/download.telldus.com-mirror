-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.0.104-2
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7)
Checksums-Sha1: 
 c92084c804c8b3db3c5533f903ce778445a97f37 71373 telldus-core_2.0.104.orig.tar.gz
 d3fbc310345beb345c436cdb43cf6fea27ea7eae 8407 telldus-core_2.0.104-2.debian.tar.gz
Checksums-Sha256: 
 959ab56f4038cb3a148d8c364c7ff57fe19c99cb12ba340be9607fde28cd431d 71373 telldus-core_2.0.104.orig.tar.gz
 cfb8cdf3ce18a9f08820083ca3e7883c63aeea4590f73bcf643d7a172fd154b9 8407 telldus-core_2.0.104-2.debian.tar.gz
Files: 
 a396de5e8664f54f2a5c718ef6c078f2 71373 telldus-core_2.0.104.orig.tar.gz
 cc8e683597f7eaea4fa65f25d26dc85d 8407 telldus-core_2.0.104-2.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk2DXZ8ACgkQWpSRgeUB6nbM1ACeP4JNcJMm9Wg9HmUYnp/Ovyny
FuoAn2vRUlOpITtZ48lfapqx2PINf6Eh
=jPan
-----END PGP SIGNATURE-----
