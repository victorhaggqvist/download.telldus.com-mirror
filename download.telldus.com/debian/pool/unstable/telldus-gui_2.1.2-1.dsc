-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-gui
Binary: libtelldus-gui2, tellduscenter
Architecture: any
Version: 2.1.2-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.com
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 7.0.50~), cmake
Package-List: 
 libtelldus-gui2 deb libs extra
 tellduscenter deb utils extra
Checksums-Sha1: 
 07ebe55b650e47c614260092d5e7459a8990d23e 1279973 telldus-gui_2.1.2.orig.tar.gz
 36306a25d2339c2ec6713038002e5eb806ca40c9 2833 telldus-gui_2.1.2-1.debian.tar.gz
Checksums-Sha256: 
 56e2fb6e7ddb60e080044a6fea60aaacb901699d9ed7fd0cb4cde370f0d6e808 1279973 telldus-gui_2.1.2.orig.tar.gz
 50b65c5ff38c1188df98876628c409d975516e8d6e9724f392aa6ecca7b7f1dd 2833 telldus-gui_2.1.2-1.debian.tar.gz
Files: 
 97e5f5cd93fd72a04aae2e3d52e6090a 1279973 telldus-gui_2.1.2.orig.tar.gz
 0447a91ec0b4a808c731bb5364aa7a17 2833 telldus-gui_2.1.2-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iEYEARECAAYFAlMHZWwACgkQWpSRgeUB6na46QCbBgzF7/2yVwtSdBkq7jf0ep55
QmUAoMWEqA9FSmiOleMAgvXjpMZD808+
=d6YC
-----END PGP SIGNATURE-----
