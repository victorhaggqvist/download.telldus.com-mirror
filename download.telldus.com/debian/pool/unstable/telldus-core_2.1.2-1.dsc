-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.1.2-1
Maintainer: Telldus Technologies AB <info.tech@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 7)
Package-List: 
 libtelldus-core-dev deb libdevel extra
 libtelldus-core2 deb libs extra
 telldus-core deb libs extra
Checksums-Sha1: 
 55be416f212f5ba561ce455b054b5d0fa6f4672a 169870 telldus-core_2.1.2.orig.tar.gz
 1b9af9c1334cdd4c759f4e37bc97b367f5519623 6736 telldus-core_2.1.2-1.debian.tar.gz
Checksums-Sha256: 
 30014638688747062c8abf901120a7ec3fb6d5addeaee2d4a86849ea15dfff5a 169870 telldus-core_2.1.2.orig.tar.gz
 d7e02118d7ccc5688fe105099d53e17565a2ef75da3e028607fe80c896bf4cea 6736 telldus-core_2.1.2-1.debian.tar.gz
Files: 
 66f36a49b9744af9fce1310eb7d1394b 169870 telldus-core_2.1.2.orig.tar.gz
 b03b4fd8b802d06218e0ab0de227daa1 6736 telldus-core_2.1.2-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iEYEARECAAYFAlMHX60ACgkQWpSRgeUB6nbllgCg0gkNWrSMGkg5gDEEqng2ZOoS
kUYAn1v3xKNHeoVjXHox16GLtCo4hJF+
=bEx5
-----END PGP SIGNATURE-----
