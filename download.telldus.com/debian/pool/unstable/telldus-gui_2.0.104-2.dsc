-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: telldus-gui
Binary: libtelldus-gui2, tellduscenter
Architecture: any
Version: 2.0.104-2
Maintainer: Telldus Technologies AB <info.tech@telldus.com>
Homepage: http://www.telldus.com
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 7.0.50~), cmake
Checksums-Sha1: 
 82e9cd9a97e224dcd6d5c7db05b31867f7b167ec 953270 telldus-gui_2.0.104.orig.tar.gz
 17ed882174df9d6d7de4fc921d3ed8eeb1d51285 9141 telldus-gui_2.0.104-2.debian.tar.gz
Checksums-Sha256: 
 0fcf9645bd2a38b04e07fdc09d7999bd6a64db5832a812c771fdf6b38a6f784f 953270 telldus-gui_2.0.104.orig.tar.gz
 2a76c68775b0d3338a0ad7feeb71016a2ae09fcb559975230554d37a47141b68 9141 telldus-gui_2.0.104-2.debian.tar.gz
Files: 
 9a55199d33539469781fef9231255ee9 953270 telldus-gui_2.0.104.orig.tar.gz
 ce5c2d7a2003222c74ee46b92f4755e8 9141 telldus-gui_2.0.104-2.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk2DOpEACgkQWpSRgeUB6naM5ACfYux/ct6X54CEzR9hxOs5sf+H
6f0AoMseeWCkhx6fAKeJfIL3k611FZ2y
=g47d
-----END PGP SIGNATURE-----
