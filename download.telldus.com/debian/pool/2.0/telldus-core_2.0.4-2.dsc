Format: 1.0
Source: telldus-core
Binary: libtelldus-core-dev, libtelldus-core2, telldus-core
Architecture: any
Version: 2.0.4-2
Maintainer: Micke Prag <micke.prag@telldus.se>
Homepage: http://www.telldus.se
Standards-Version: 3.7.3
Build-Depends: debhelper (>= 7)
Checksums-Sha1: 
 5762548c4707e4d1bc276e304ac75ed9b89de5aa 71884 telldus-core_2.0.4.orig.tar.gz
 8231d30044f02eb2d6acb96c81923a91dcf3d024 15833 telldus-core_2.0.4-2.diff.gz
Checksums-Sha256: 
 e14a128b44c6207c1b54303e942af599e22527992e865c022c888578f0c94b3a 71884 telldus-core_2.0.4.orig.tar.gz
 51935193924c98f29d5ee3f2382d809fc24b0b585ecf05e1b8e7c6835ee58b0e 15833 telldus-core_2.0.4-2.diff.gz
Files: 
 adf1994829b5bd1015d8f172b654f309 71884 telldus-core_2.0.4.orig.tar.gz
 2749775fd02a8bf526e40f5552cc872b 15833 telldus-core_2.0.4-2.diff.gz
